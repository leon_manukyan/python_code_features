a, b = 1, 2

# Expected result:
# {
#     'statements' : {},
#     'functions'  : {},
#     'imports'    : {},
#     'expressions': {},
#     'constructs' : {'Assignment' : 1, 'MultiTargetAssignment' : 1}
# }
