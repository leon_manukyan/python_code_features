def count_evens(i):
    return len([x for x in i if x % 2 == 0])

# Expected result:
# {
#     'statements' : {'Return': 1},
#     'functions'  : {'len': True},
#     'imports'    : {},
#     'expressions': {'Eq' : 1, 'Mod': 1},
#     'constructs' : {
#                      'FunctionDef' : 1,
#                      'Comprehension' : 1,
#                      'ListDisplay' : 1,
#                      'ListComprehension' : 1,
#                      'FilteredComprehension' : 1
#                    }
# }
