{x:1 for _ in somelist for _ in otherlist}

# Expected result:
# {
#     'statements' : {},
#     'functions'  : {},
#     'imports'    : {},
#     'expressions': {},
#     'constructs' : {
#                      'Comprehension' : 1,
#                      'DictionaryDisplay' : 1,
#                      'DictionaryComprehension' : 1,
#                      'MultilevelComprehension' : 1
#                    }
# }
