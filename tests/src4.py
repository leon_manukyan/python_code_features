import os
import numpy as np
for val in "string":
    if val == "i":
        break
    print(val)

print("The end")

# Expected result:
# {
#     'statements': {'For': 1, 'If': 1, 'Break': 1},
#     'functions': {'print': 2},
#     'imports': {'numpy': True, 'os': True},
#     'expressions': {'Eq': 1},
#     'constructs' : {}
# }
