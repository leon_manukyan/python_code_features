foo(1, a=2)

# Expected result:
# {
#     'statements' : {},
#     'functions'  : {'foo': True},
#     'imports'    : {},
#     'expressions': {},
#     'constructs' : {'KeywordArgumentUsage' : 1}
# }
