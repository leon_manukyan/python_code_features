a if a > b else b

# Expected result:
# {
#     'statements' : {},
#     'functions'  : {},
#     'imports'    : {},
#     'expressions': { 'Gt' : 1},
#     'constructs' : {'IfExpression' : 1}
# }
