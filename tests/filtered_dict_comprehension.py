{x:1 for x in somelist if x > 0}

# Expected result:
# {
#     'statements' : {},
#     'functions'  : {},
#     'imports'    : {},
#     'expressions': { 'Gt' : 1 },
#     'constructs' : {
#                      'Comprehension' : 1,
#                      'DictionaryDisplay' : 1,
#                      'DictionaryComprehension' : 1,
#                      'FilteredComprehension' : 1
#                    }
# }
