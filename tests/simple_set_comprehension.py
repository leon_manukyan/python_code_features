{x for x in somelist}

# Expected result:
# {
#     'statements' : {},
#     'functions'  : {},
#     'imports'    : {},
#     'expressions': {},
#     'constructs' : {
#                      'Comprehension' : 1,
#                      'SetDisplay' : 1,
#                      'SetComprehension' : 1
#                    }
# }
