(x for _ in somelist for _ in otherlist)

# Expected result:
# {
#     'statements' : {},
#     'functions'  : {},
#     'imports'    : {},
#     'expressions': {},
#     'constructs' : {
#                      'Comprehension' : 1,
#                      'GeneratorExpression' : 1,
#                      'MultilevelComprehension' : 1
#                    }
# }
