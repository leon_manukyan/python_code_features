import sys, json
import numpy.linalg

# Expected result:
# {
#     'statements' : {},
#     'functions'  : {},
#     'imports'    : {'sys' : True, 'json' : True, 'numpy.linalg' : True},
#     'expressions': {},
#     'constructs' : {}
# }
