import pandas as pd
import numpy.linalg as la

# Expected result:
# {
#     'statements' : {},
#     'functions'  : {},
#     'imports'    : {'numpy.linalg' : True, 'pandas' : True},
#     'expressions': {},
#     'constructs' : {}
# }
