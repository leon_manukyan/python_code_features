def f1(l):
    l.reverse()
    reverse(l)
    l.sort()
    sorted(l)
    len(l)
    for x in l:
        pass
    return(l)

# Expected result:
# {
#     'statements': {'For': 1, 'Return': 1, 'Pass': 1},
#     'functions': {'reverse': 2, 'sort': 1, 'sorted': 1, 'len': 1},
#     'imports': {},
#     'expressions': {},
#     'constructs' : {'FunctionDef' : 1}
# }
