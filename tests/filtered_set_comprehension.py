{x for x in somelist if x > 0}

# Expected result:
# {
#     'statements' : {},
#     'functions'  : {},
#     'imports'    : {},
#     'expressions': { 'Gt' : 1 },
#     'constructs' : {
#                      'Comprehension' : 1,
#                      'SetDisplay' : 1,
#                      'SetComprehension' : 1,
#                      'FilteredComprehension' : 1
#                    }
# }
