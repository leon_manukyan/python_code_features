[x for x in somelist]

# Expected result:
# {
#     'statements' : {},
#     'functions'  : {},
#     'imports'    : {},
#     'expressions': {},
#     'constructs' : {
#                      'Comprehension' : 1,
#                      'ListDisplay' : 1,
#                      'ListComprehension' : 1
#                    }
# }
