import Math
def f1(l):
    x = 0 if l else 10
    while x < 5:
        x += 1
    return(l)

# Expected result:
# {
#     'statements': {'While': 1, 'Return': 1},
#     'functions': {},
#     'imports': {'Math': True},
#     'expressions': {'Lt': 1},
#     'constructs' : {
#                       'FunctionDef' : 1,
#                       'IfExpression' : 1,
#                       'Assignment' : 1,
#                       'AugmentedAssignment' : 1
#                    }
# }
