{x:1 for x in somelist}

# Expected result:
# {
#     'statements' : {},
#     'functions'  : {},
#     'imports'    : {},
#     'expressions': {},
#     'constructs' : {
#                      'Comprehension' : 1,
#                      'DictionaryDisplay' : 1,
#                      'DictionaryComprehension' : 1
#                    }
# }
