def f1(l):
    l.reverse()
    if(true):
      return l
    return l

# Expected result:
# {
#     'statements' : {'If': 1, 'Return': 2},
#     'functions'  : {'reverse': True},
#     'imports'    : {},
#     'expressions': {},
#     'constructs' : {'FunctionDef' : 1}
# }
