[x for x in somelist if x > 0]

# Expected result:
# {
#     'statements' : {},
#     'functions'  : {},
#     'imports'    : {},
#     'expressions': { 'Gt' : 1 },
#     'constructs' : {
#                      'Comprehension' : 1,
#                      'ListDisplay' : 1,
#                      'ListComprehension' : 1,
#                      'FilteredComprehension' : 1
#                    }
# }
