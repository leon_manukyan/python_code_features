a[1:2:3]

# Expected result:
# {
#     'statements' : {},
#     'functions'  : {},
#     'imports'    : {},
#     'expressions': {},
#     'constructs' : {'Subscription' : 1, 'Slicing' : 1}
# }
