from sys import argv
from ast import *
from os import unlink as rm

# Expected result:
# {
#     'statements' : {},
#     'functions'  : {},
#     'imports'    : {'sys.argv' : True, 'ast.*' : True, 'os.unlink' : True},
#     'expressions': {},
#     'constructs' : {}
# }
