foo(1, 2)

# Expected result:
# {
#     'statements' : {},
#     'functions'  : {'foo': True},
#     'imports'    : {},
#     'expressions': {},
#     'constructs' : {}
# }
