1 < x < y <= 5

# Expected result:
# {
#     'statements' : {},
#     'functions'  : {},
#     'imports'    : {},
#     'expressions': {'Lt' : 2, 'LtE' : 1},
#     'constructs' : {'ChainedCompare' : 1}
# }
